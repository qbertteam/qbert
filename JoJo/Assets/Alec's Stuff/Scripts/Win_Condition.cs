﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win_Condition : MonoBehaviour {

    public GameObject[] CubeList;
    public GameObject Player;
    public int CubeColor;
    private Scene currentscene;
    public Color win;
    public GameObject winpanel;

    // Use this for initialization
    void Start () {
        CubeColor = 0;
        winpanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(CubeColor == 28)
        {
            int temp = currentscene.buildIndex;
            temp++;
            SceneManager.LoadScene(temp);
            

        }
       
	}
 /*   private void OnCollisionEnter(Collision collision)
    {
        if (gameObject.tag == "Player")
        {
            CubeColor++;
            if( CubeColor == 42)
            {
                winpanel.SetActive(true);
            }
            else
            {
                winpanel.SetActive(false);
            }
        }
           
    } */
}
