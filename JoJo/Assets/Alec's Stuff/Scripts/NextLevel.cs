﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {
    [SerializeField]
    bool PreyInRange;
    [SerializeField]
    GameObject Char;
	private Scene currentscene;
	//int sceneindex = 3;

	// Use this for initialization
	void OnTriggerEnter(Collider other)
	{
        if (other.gameObject == Char)
        {
            PreyInRange = true;

            currentscene = SceneManager.GetActiveScene();
            int temp = currentscene.buildIndex;
            temp++;
            SceneManager.LoadScene(temp);
        }
	}
}
