﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	public void RestartGame ()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		Time.timeScale = 1;
	}
	public void Credit()
	{
		SceneManager.LoadScene ("Credits");	
	}
	public void MenuReturn ()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene ("menu screen");
	}

	public void LevelOne()
	{
		SceneManager.LoadScene ("Level1");
	}
	public void LevelTwo()
	{
		SceneManager.LoadScene ("scene 2");
	}
	public void LevelThree()
	{
		SceneManager.LoadScene ("scene 3");
	}
    public void MenuScreenReturn()
    {
        SceneManager.LoadScene("menu screen");
    }
}