﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public GameObject Qbert;
    public Vector3 moveRightUp;
    public Vector3 moveLeftUp;
    public Vector3 moveRightDown;
    public Vector3 moveLeftDown;
    public Vector3 currentPosition;
    public Vector3 turnForwardRight;
    public Vector3 turnForwardLeft;
    public Vector3 turnBackwardsRight;
    public Vector3 turnBackwardsLeft;
    public Vector3 currentRotation;
    public int ScoreWin = 0;
    

    // Use this for initialization
    void Start()
    {
        currentRotation = Qbert.transform.rotation.eulerAngles;

        turnForwardRight = transform.rotation.eulerAngles;
        turnForwardRight.x = 0.0f;
        turnForwardRight.y = 180.0f;
        turnForwardRight.z = 0.0f;

        turnForwardLeft = transform.rotation.eulerAngles;
        turnForwardLeft.x = 0.0f;
        turnForwardLeft.y = -90.0f;
        turnForwardLeft.z = 0.0f;

        turnBackwardsRight = transform.rotation.eulerAngles;
        turnBackwardsRight.x = 0.0f;
        turnBackwardsRight.y = 90.0f;
        turnBackwardsRight.z = 0.0f;

        turnBackwardsLeft = transform.rotation.eulerAngles;
        turnBackwardsLeft.x = 0.0f;
        turnBackwardsLeft.y = 0.0f;
        turnBackwardsLeft.z = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("floor").Length >= 28)
        {
            print("win");
        }

        Qbert.transform.eulerAngles = currentRotation;
        currentPosition = Qbert.transform.position;
        moveRightUp = currentPosition + new Vector3(-1, 1, 0);
        moveLeftUp = currentPosition + new Vector3(0, 1, -1);
        moveLeftDown = currentPosition + new Vector3(1, -1, 0);
        moveRightDown = currentPosition + new Vector3(0, -1, 1);

        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Qbert.transform.position = moveRightUp;
                currentRotation = turnBackwardsRight;
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                Qbert.transform.position = moveLeftUp;
                currentRotation = turnBackwardsLeft;
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                Qbert.transform.position = moveLeftDown;
                currentRotation = turnForwardLeft;
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                Qbert.transform.position = moveRightDown;
                currentRotation = turnForwardRight;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "floor")
        {
            
        }
    }
    
}
