﻿//Developer: Alec Henderson
//Contributions: All
//Start And End Dates: 11/29/2017 - 12/8/2017
//References:
//Links:

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {


    private Scene currentscene;
    public void RestartGame ()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		Time.timeScale = 1;
	}
	public void Credit()
	{
		SceneManager.LoadScene ("Credits");	
	}
	public void MenuReturn ()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene ("qbertmenu");
	}

	public void LevelOne()
	{
		SceneManager.LoadScene ("Level1");
	}
	public void LevelTwo()
	{
		SceneManager.LoadScene ("scene 2");
	}
	public void LevelThree()
	{
		SceneManager.LoadScene ("scene 3");
	}
    public void MenuScreenReturn()
    {
        SceneManager.LoadScene("qbertmenu");

    }
    public void NextLevel()
    {
        currentscene = SceneManager.GetActiveScene();
        int temp = currentscene.buildIndex;
        temp++;
        SceneManager.LoadScene(temp);

    }
}