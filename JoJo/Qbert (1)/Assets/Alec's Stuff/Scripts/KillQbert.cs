﻿//Developer: Alec Henderson
//Contributions: All but putting Rigid body on trigger enter
//Start And End Dates: 11/29/2017 - 12/8/2017
//References:Sang Tiet
//Links:
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillQbert : MonoBehaviour
{
    public int life;
    public GameObject Qbert;
    public GameObject QB1;
    public GameObject QB2;
    public GameObject GAMEOVER;
    public Vector3 reset;
    public Rigidbody rb;



    // Use this for initialization
    void Start()
    {
        rb = Qbert.GetComponent<Rigidbody>();
        reset = new Vector3(-6, 7, 0);

       // GAMEOVER.SetActive(false);
        life = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (life == 3)
        {
            QB1.SetActive(true);
            QB2.SetActive(true);


        }
    }
    void GameOver()
    {
 
        SceneManager.LoadScene("GameOver");
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            life--;
            if (life == 2)
            {
                QB1.SetActive(false);
            }
            if (life == 1)
            {
                QB2.SetActive(false);
            }
            else if (life == 0)
            {
                GameOver();
            }
           
        }
        if (collision.gameObject.tag == "Death")
        {
            Qbert.transform.position = reset;
            rb.velocity = (transform.forward * 0) + (transform.up * 0);
            Qbert.GetComponent<Movement>().enabled = true;
            //rb.useGravity = false;


            life--;
            if (life == 2)
            {
                QB1.SetActive(false);
            }
            if (life == 1)
            {
                QB2.SetActive(false);
            }
            else if (life == 0)
            {
                GameOver();
            }
        }
    }
}
        
    

