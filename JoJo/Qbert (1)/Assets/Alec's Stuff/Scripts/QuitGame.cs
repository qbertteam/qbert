﻿//Developer: Alec Henderson
//Contributions: All
//Start And End Dates: 11/29/2017 - 12/8/2017
//References:
//Links:

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour {

	public void ClickExit()
	{
		Application.Quit();
	}
}