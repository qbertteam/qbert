﻿//Developer: Alec Henderson
//Contributions: All
//Start And End Dates: 11/29/2017 - 12/8/2017
//References:
//Links:
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class YouDidItSneaky : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter()
	{
		SceneManager.LoadScene ("menu screen");
	}
}
