﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*        
//        Developer Name:Shamar Winston
//         Contribution: Coily Ai in which I Shamar wrote the entire code 
//                Feature Coily AI movemnet 
//                Start & End dates 12-5-17 to 12-8-17 
//                References: Classmate Deven 
//                        Links:
//*/

public class SW_CoilyAI : MonoBehaviour
{

    public float movementTimer = 0;

    private float currentTime = 1;


    bool left = false;
    bool right = false;
    bool up = false;
    bool down = false;

    public Transform leftCube;
    public Transform rightCube;
    public Transform topCube;
    public Transform downCube;
    private int LifeisBad;
    public GameObject GameOverDeathCanvas;
    public Transform respawnPosition;
    

     GameObject player;

    // Use this for initialization
    void Start()
    {
        GameOverDeathCanvas = GameObject.FindGameObjectWithTag("Death");
        respawnPosition = GameObject.FindGameObjectWithTag("Respawn").transform;
        player = /*FindObjectOfType<Movement>().gameObject;*/ GameObject.FindGameObjectWithTag("Player");
        //GameOverDeathCanvas.SetActive(false);
       // print("start");
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        print("update");
        if (currentTime >= movementTimer)
        {
            //Move towards players
            currentTime = 0;
            Vector3 direction = -1*(this.transform.position - player.transform.position);
            direction.Normalize();
            Debug.Log(direction);

            if (direction.y > 0.1 && direction.x < -0.1)
            {
                //player is above
                Debug.Log("UP");
                transform.position += topCube.localPosition + new Vector3(0, 1, 0);

            }
            else if (direction.y < -1 && direction.z < -0.1 && right)
            {
                //player is on the right
                Debug.Log("RIGHT");
                transform.position += rightCube.localPosition - new Vector3(0, 1, 0);
                
            }
           else if (direction.y > 1  && direction.z > 1 )/*left)*/
            {
                //player is on the left
                Debug.Log("LEFT");
                transform.position += leftCube.localPosition + new Vector3(0, 1, 0);
                
            }
           else if (direction.y < -0.1 && direction.z < -0.1 /*down*/)
            {
                //player is below
                Debug.Log("DOWN");
                transform.position += downCube.localPosition;/* - new Vector3(0, 1, 0);*/

                
            }
        }

        
        {
            gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    public void setUp()
    {
        up = true;
    }
    public void setDown()
    {
        down = true;
    }
    public void setRight()
    {
        right = true;
    }
    public void setLeft()
    {
        left = true;
    }

    public void notsetUp()
    {
        up = false;
    }
    public void notsetDown()
    {
        down = false;
    }
    public void notsetRight()
    {
        right = false;
    }
    public void notsetLeft()
    {
        left = false;
    }


}
