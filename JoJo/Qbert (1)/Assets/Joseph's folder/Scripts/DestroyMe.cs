﻿//Joseph Johnston
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour {
    public GameObject Me;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        

    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Despawn")
        {
            Destroy(Me);
        }
    }
}
