﻿//Joseph Johnston//
//Developer: Joseph Johnston/ Alec Henderson/Shamar Winston
//Contributions: Win Condition - Alec Henderson, Sounds for Q'Bert movemnet sounds -Shamar Winston 
//Start And End Dates: 11/29/2017 - 12/8/2017
//References:
//Links:
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Movement : MonoBehaviour
{
    public GameObject Qbert;
    public Vector3 moveRightUp;
    public Vector3 moveLeftUp;
    public Vector3 moveRightDown;
    public Vector3 moveLeftDown;
    public Vector3 currentPosition;
    public Vector3 turnForwardRight;
    public Vector3 turnForwardLeft;
    public Vector3 turnBackwardsRight;
    public Vector3 turnBackwardsLeft;
    public Vector3 currentRotation;
    public int ScoreWin = 0;
    public AudioSource Step;
    private Scene currentscene;
    public GameObject Confetti;
    public float conffettitimer;
    public float waitfor;
    public Rigidbody QB;
    private float timer;
    public GameObject QbertChild;


    // Use this for initialization
    void Start()
    {
        waitfor = 2;
        QB = Qbert.GetComponent<Rigidbody>();
        
    }


    // Update is called once per frame
    void Update()
    {
        //Alec Henderson
        timer += Time.deltaTime;
        //conffettitimer += Time.deltaTime;
        if (GameObject.FindGameObjectsWithTag("floor").Length >= 28)
        {
            conffettitimer += Time.deltaTime;
            timeawaken();   
            Confetti.SetActive(true);
            if (conffettitimer >= waitfor){
                currentscene = SceneManager.GetActiveScene();
                int temp = currentscene.buildIndex;
                temp++;
                SceneManager.LoadScene(temp);

            }


        }

        //Qbert.transform.eulerAngles = currentRotation;
        //currentPosition = Qbert.transform.position;
        //moveRightUp = currentPosition + new Vector3(-1, 1, 0);
        //moveLeftUp = currentPosition + new Vector3(0, 1, -1);
        //moveLeftDown = currentPosition + new Vector3(1, -1, 0);
        //moveRightDown = currentPosition + new Vector3(0, -1, 1);

        {
            if (Input.GetKeyDown(KeyCode.A) && timer >= 1)
            {
                QB.velocity = (transform.forward * 1) + (transform.up * 6);
                
                timer = 0;
                GetComponent<AudioSource>().Play();
                QbertChild.transform.eulerAngles = new Vector3(0, 0, 0);
            }

            if (Input.GetKeyDown(KeyCode.S) && timer >= 1)
            {
                
                QB.velocity = (transform.right * -1f) + (transform.up * 4);
                //Qbert.transform.position = moveLeftUp;
                timer = 0;
                GetComponent<AudioSource>().Play();
                QbertChild.transform.eulerAngles = new Vector3(0, 270, 0);
            }

            if (Input.GetKeyDown(KeyCode.D) && timer >= 1)
            {
                
                QB.velocity = (transform.forward * -1f) + (transform.up * 4);
                //Qbert.transform.position = moveLeftDown;
                timer = 0;
                GetComponent<AudioSource>().Play();
                QbertChild.transform.eulerAngles = new Vector3(0, 180, 0);
            }

            if (Input.GetKeyDown(KeyCode.W) && timer >= 1)
            {
                QB.velocity = (transform.right * 1) + (transform.up * 6);
                //Qbert.transform.position = moveRightDown;
                timer = 0;
                GetComponent<AudioSource>().Play();
                QbertChild.transform.eulerAngles = new Vector3(0, 90, 0);
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "floor")
        {
            
        }
    }
 public void timeawaken()
    {
        //conffettitimer = 0;
        //conffettitimer += Time.deltaTime;
    }   
}
