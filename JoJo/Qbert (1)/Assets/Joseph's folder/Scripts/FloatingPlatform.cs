﻿//Joseph Johnston
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingPlatform : MonoBehaviour {
    private bool PlayerHasTag = false;
    public GameObject Platform;
    private Animator platformAnim;
    public GameObject Qbert;
    public Rigidbody rb;

	// Use this for initialization
	void Start () {
        platformAnim = Platform.GetComponent<Animator>();
        rb = Qbert.GetComponent<Rigidbody>();
	}


    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Qbert.transform.parent = Platform.transform;
            if (platformAnim.GetBool("PlayerHasTag") == false)
            {
                platformAnim.SetBool("PlayerHasTag", true);
            }
        }
    }


    void OnTriggerExit(Collider other)
    {
        Qbert.transform.parent = null;
        rb.useGravity = true;
        Destroy(Platform, 1);
    }


	// Update is called once per frame
	void Update () {
		
	}
}
