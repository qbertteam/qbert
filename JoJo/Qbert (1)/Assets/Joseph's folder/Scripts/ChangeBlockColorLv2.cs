﻿//Joseph Johnston//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBlockColorLv2 : MonoBehaviour
{

    public Material[] material;
    Renderer Rend;
    public int matNum;
    public GameObject block;
    public int HitsToChange;
    public int MatNumMax;

    void Start()
    {
        Rend = GetComponent<Renderer>();
        Rend.enabled = true;
        Rend.sharedMaterial = material[0];
        matNum = 1;
    }
    void Update()
    {
        if (HitsToChange == 0)
        {
            block.tag = "floor";
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        
            {
                if (matNum <= MatNumMax)
                {
                    Rend.sharedMaterial = material[matNum];
                    matNum = matNum + 1;
                }
                if (HitsToChange > 0)
                {
                    HitsToChange = HitsToChange - 1;
                }

            }
        
        //else
        //{
        //Rend.sharedMaterial = material[2];
        //}
    }
}
