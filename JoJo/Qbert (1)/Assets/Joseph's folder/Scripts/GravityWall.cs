﻿//Joseph Johnston
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityWall : MonoBehaviour {
    public GameObject ObjectToGrav;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = ObjectToGrav.GetComponent<Rigidbody>();
        //rb.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Gravity" && ObjectToGrav.GetComponent<Movement>().enabled == true)
        {
            rb.useGravity = true;
            ObjectToGrav.GetComponent<Movement>().enabled = false;
        }

        else if (collision.gameObject.tag == "Gravity" && ObjectToGrav.GetComponent<Movement>().enabled == false)
        {
            rb.useGravity = true;
            ObjectToGrav.GetComponent<Movement>().enabled = true;
        }
    }

}
