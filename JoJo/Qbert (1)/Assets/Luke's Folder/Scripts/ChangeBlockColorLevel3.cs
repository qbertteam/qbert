﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBlockColorLevel3 : MonoBehaviour
/*
// Developer Name: Luke Fritz
// Contobution: Wrote Block Change Script
// Feature: Changes the color of the Blocks so they switch eveytime Qbert steps on them
// State & End Dates: Dec. 6
// Reference: Scripting: Change Material of an Object | Unity Tutorial
// Link: https://www.youtube.com/watch?v=dJB07ZSiW7k
*/
{

    public Material[] material;
    Renderer Rend;
    public int matNum;
    public GameObject block;
    public int arraySize;

    void Start()
    {
        Rend = GetComponent<Renderer>();
        Rend.enabled = true;
        Rend.sharedMaterial = material[0];
        matNum = 1;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (matNum == 0)
            {
                Rend.sharedMaterial = material[matNum];
                matNum = 1;
                if (arraySize <= 2)
                    block.tag = "Untagged";
            }
            else if (matNum == 1)
            {
                Rend.sharedMaterial = material[matNum];
                matNum = 0;
                if (arraySize <= 2)
                    block.tag = "floor";
            }


        }
        //else
        //{
        //Rend.sharedMaterial = material[2];
        //}
    }
}
